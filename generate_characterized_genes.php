<?php

$ticket = 'DB-515';
if (isset($argv[1])) {
    $ticket = $argv[1];
}

$paramContents = file_get_contents(__DIR__.'/parameters.json');
$params = json_decode($paramContents, true);
$coreConn = new mysqli($params["core_database_host"], $params["core_database_user"], $params["core_database_password"], $params["core_database_name"]);

if($coreConn->connect_error){
    throw new \Exception($coreConn->connect_error);
}

$characterizedStmt = '
    SELECT g.`gene_id`, g.`symbol`
    FROM `gene_characterizations` gc

    # Sub-query - group by gene, then pick the max id
    INNER JOIN (
        SELECT MAX(`id`) AS max_id
        FROM `gene_characterizations`
        GROUP BY `gene_id`
    ) AS x ON gc.`id` = x.`max_id`

    # Join to characterizations and only keep the known ones
    INNER JOIN `characterizations` c ON gc.`characterization_id` = c.`id`
        AND c.`is_known` = 1

    # Join to genes so can find the symbol
    INNER JOIN `genes` g ON gc.`gene_id` = g.`id`

    # Order by symbol ascending
    ORDER BY g.`symbol` ASC
';

$res = $coreConn->query(trim($characterizedStmt));
$queriedGenes = $res->fetch_all();

$characterized = [];
foreach ($queriedGenes as $row) {
    $characterized[$row[0]] = $row[1];
}

$interestStmt = '
    SELECT g.`gene_id`, g.`symbol`
    FROM `gene_characterizations` gc

    # Sub-query - group by gene, then pick the max id
    INNER JOIN (
        SELECT MAX(`id`) AS max_id
        FROM `gene_characterizations`
        GROUP BY `gene_id`
    ) AS x ON gc.`id` = x.`max_id`

    # Join to characterizations and only keep the "genes of interest"
    INNER JOIN `characterizations` c ON gc.`characterization_id` = c.`id`
        AND c.`name` = "gen-int"

    # Join to genes so can find the symbol
    INNER JOIN `genes` g ON gc.`gene_id` = g.`id`

    # Order by symbol ascending
    ORDER BY g.`symbol` ASC
';

$res = $coreConn->query(trim($interestStmt));
$queriedGenes = $res->fetch_all();

$interest = [];
foreach ($queriedGenes as $row) {
    $interest[$row[0]] = $row[1];
}

$coreConn->close();

$timestamp = new \DateTime;

$data = array(
    'comment' => "This file was generated in $ticket at the timestamp indicated.",
    'timestamp' => $timestamp->format('c'),
    'characterized_genes' => $characterized,
    'genes_of_interest' => $interest,
);

$fp = fopen('resources/characterized_genes.json', 'w');
fwrite($fp, json_encode($data, JSON_PRETTY_PRINT));
fclose($fp);
